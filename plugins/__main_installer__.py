#!/usr/bin/env python

#
#    MiMiCPy: Python Based Tools for MiMiC
#    Copyright (C) 2020-2023 Bharath Raghavan,
#                            Florian Schackert
#
#    This file is part of MiMiCPy.
#
#    MiMiCPy is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    MiMiCPy is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from os import path
import sys
import platform
from shutil import copyfileobj
import argparse

def copy_script(source_script, dest):
    # assume the plugin scripts are in the same directory as this script
    source = path.join(path.dirname(path.realpath(__file__)), source_script)
    if not path.isfile(source):
        print("Cannot find plugin file {}".format(source))
        sys.exit(1)
    else:
        with open(source, 'r') as fsrc, open(dest, 'a') as fdest:
            copyfileobj(fsrc, fdest)
                
def main():
    print("Program to install the PrepQM plugins bundled with MiMiCPy")
    parser = argparse.ArgumentParser(prog='mimicpy_plugin_installer')
    parser.add_argument('-vmddir', help='path to the PrepQM VMD plugin directory')
    parser.add_argument('-pymoldir', help='path to the PrepQM PyMOL plugin directory')
    args = parser.parse_args()
    
    pymol_dir = args.pymoldir
    vmd_dir = args.vmddir
    
    if pymol_dir:
        if platform.system() == 'Windows':
            pymolrc = path.join(pymol_dir, 'pymolrc.py')
        else:
            pymolrc = path.join(pymol_dir, '.pymolrc.py')
        
        copy_script('pymol.py', pymolrc)
        print("Wrote the PrepQM PyMOL plugin to {}".format(pymolrc))

    if vmd_dir:
        if platform.system() == 'Windows':
            vmdrc = path.join(vmd_dir, 'vmd.rc')
        else:
            vmdrc = path.join(vmd_dir, '.vmdrc')

        copy_script('vmd.tcl', vmdrc)
        print("Wrote the PrepQM VMD plugin to file {}".format(vmdrc))
    
    if not pymol_dir and not vmd_dir:
        print("Error! At least one plugin directory should be specified")
    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.exit(1)