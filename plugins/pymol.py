#
#    MiMiCPy: Python Based Tools for MiMiC
#    Copyright (C) 2020-2023 Bharath Raghavan,
#                            Florian Schackert
#
#    This file is part of MiMiCPy.
#
#    MiMiCPy is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    MiMiCPy is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import mimicpy
from pymol import cmd

def prepqm(top, selection, bound_selection=None, ndx='index.ndx', out='cpmd.inp', inp=None, 
           pad=0, abs=False, qma='qmatoms', path=None,  q=None, pp=None, find_bound=False):
    try:
        qm = mimicpy.Preparation(mimicpy.PyMOLSelector(top))
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        sys.exit(1)
    except (mimicpy.utils.errors.ParserError, mimicpy.utils.errors.MiMiCPyError) as e:
        print(e)
        sys.exit(1)
    
    try:
        qm.add(selection)
        
        if bound_selection != None:
            qm.add(bound_selection, True)
        
        if find_bound:
            print("Boundary atoms added automatically.")
            qm.find_bound_atoms()
            
        qm.get_mimic_input(inp, ndx, out, pad, abs, qma, path, q, pp)
    except mimicpy.utils.errors.SelectionError as e:
        print(e)
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        sys.exit(1)

cmd.extend('prepqm', prepqm)

##################################
