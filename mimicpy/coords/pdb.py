#
#    MiMiCPy: Python Based Tools for MiMiC
#    Copyright (C) 2020-2023 Bharath Raghavan,
#                            Florian Schackert
#
#    This file is part of MiMiCPy.
#
#    MiMiCPy is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    MiMiCPy is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""Module for pdb files"""

import logging

import pandas as pd

from .base import BaseCoordsClass


class Pdb(BaseCoordsClass):
    """Reads and writes PDB files
       File Format taken from:
           https://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html
    """

    @staticmethod
    def __read_line(line):
        vals = {}
        vals['x'] = float(line[30:38].strip())/10
        vals['y'] = float(line[38:46].strip())/10
        vals['z'] = float(line[46:54].strip())/10

        # Skip reading rest of the PDB file, left here for reference
        # vals['record'] = line[:6].strip()
        # vals['id'] = int(line[6:11].strip())
        # vals['name'] = line[12:16].strip()
        # vals['altLoc'] = line[16]
        # vals['resname'] = line[17:20].strip()
        # vals['chainID'] = line[21]
        # vals['resSeq'] = int(line[22:26].strip())
        # vals['iCode'] = line[26]
        # vals['occupancy'] = float(line[54:60].strip())
        # vals['tempFactor'] = float(line[60:66].strip())
        # vals['element'] = line[76:78].strip()
        # vals['charge'] = float(line[78:80].strip())

        return vals

    def _read(self):
        coords = []
        for line_number, line in enumerate(self.file.readlines()):
            if line[:6].strip() not in ['ATOM', 'HETATM']:
                logging.warning('Skipping non-atom line %s: %s', line_number, line)
                continue
            vals = self.__read_line(line)
            coords.append(vals)
        coords = pd.DataFrame(coords)
        coords['id'] = coords.index.to_numpy()+1

        return coords.set_index(['id']), None

    def _write(self, mpt_coords, box, title):

        def guess_chain(s):
            """Guess chain ID from molecule name
               It is not very accurate
            """
            try:
                chain = [l for l in s if l.isupper()][-1]
            except IndexError:
                chain = ' '

            if chain not in ['A', 'B', 'C', 'D']:
                chain = ' '

            return chain

        def name_checker(s):
            s = self.str_checker(s, 4)
            if len(s) == 3:
                return " {}".format(s)
            return "{:^4}".format(s)

        def charge_checker(i):
            i = round(i)
            if i>0:
                return "{}+".format(i)
            if i<0:
                return "{}-".format(-i)
            return "  "

        std_res = [
            'ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU',
            'GLY', 'HIS', 'HID', 'ILE', 'LEU', 'LYS', 'MET',
            'PHE', 'PRO', 'SER', 'THR', 'TRP', 'TYR', 'VAL',
            ]

        pdb_str = ''
        for i, r in mpt_coords.iterrows():
            resname = r['resname'].upper()
            if resname in std_res:
                record = 'ATOM'
            else:
                record = 'HETATM'

            pdb_str += '{:<6}{:>5d} {} {:>3} {:1}{:>4d}    {:>8.3f}{:>8.3f}{:>8.3f}  0.00  0.00          {:>2}{}\n'.format(record,\
                                                                        self.int_checker(r['id'], 5),\
                                                                        name_checker(r['name']),\
                                                                        self.str_checker(resname, 3),\
                                                                        guess_chain(r['mol']),\
                                                                        self.int_checker(r['resid'], 4),\
                                                                        r['x']*10,\
                                                                        r['y']*10,\
                                                                        r['z']*10,\
                                                                        self.str_checker(r['element'], 2),\
                                                                        charge_checker(r['charge'])
                                                                       )
        pdb_str += "TER   \n"

        if box is None:
            box = [0, 0, 0]
            for i, c in enumerate(['x', 'y', 'z']):
                box[i] = abs(max(mpt_coords[c]*10) - min(mpt_coords[c]*10)) # find box size

        if title:
            title = "TITLE     {}\n".format(title.upper())
        header = ("{}REMARK    GENERATED BY MIMICPY\n"
                  "CRYST1{:9.3f}{:9.3f}{:9.3f}{:7.3f}{:7.3f}{:7.3f} P 1           1\n".format(title, box[0], box[1], box[2], 90, 90, 90))

        return header+pdb_str
