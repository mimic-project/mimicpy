# MiMiCPy
MiMiCPy is the companion library to the MiMiC framework for streamline preparation of input files. It includes a suite of command line tools and plugins for PyMOL/VMD. Visit the [MiMiC homepage](https://mimic-project.org/) for more details and documentation.

## Installation
To install, run the following command:

```
pip install mimicpy
```

The PrepQM plugins for PyMOL and/or VMD can be optionally installed by running:

```
mimicpy_plugin_installer -pymoldir /path/to/plugin/ -vmddir /path/to/plugin/
```

The path to the plugin is usually either the path to the PyMOL/VMD installation, or the user home directory.

## Portability Issues
MiMiCPy requires Python >= 3.5, pandas >= 0.24.0 and numpy >= 1.12.0. The plugins have been tested with PyMOL version 2.3.4 and VMD version 1.9.4a38, although other versions are expected to work. If any incompatibilities are found, please post an issue [here](https://gitlab.com/MiMiC-projects/user-support).

## Demo
A demo of atom selection for the QM region, and generation of the MiMiC-based CPMD input file using MiMiCPy is shown below.
```console
user@system:~$ mimicpy prepqm -top acetone.top -coords acetone.gro


 	                ***** MiMiCPy *****

 	 For more information type mimicpy [subcommand] --help

=====> Running prepqm <=====


**Reading topology**

Cannot find path to Gromacs installation.
Read atoms from acetone.itp.
No atoms found in acetone.top.

Some atom types had no atom numbers information.
They were guessed as follows:

+---------------------+
| Atom Type | Element |
+---------------------+
|     c     |    C    |
+---------------------+
|     c3    |    C    |
+---------------------+
|     o     |    O    |
+---------------------+
|     hc    |    H    |
+---------------------+

**Reading coordinates**  |Done

Please enter selection below. For more information type 'help'
> add resname is ACT
> q
Using default values for maxstep and timestep
Wrote Gromacs index file to index.ndx
Wrote new CPMD input script to cpmd.inp

=====> Done <=====

```